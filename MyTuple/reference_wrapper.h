#pragma once
#include <cstddef>

namespace My {
	template<class T>
	class reference_wrapper
	{
		T* t;
	public:
		reference_wrapper() : t()
		{

		}

		reference_wrapper(const reference_wrapper& wr) : t(wr.t)
		{

		}

		reference_wrapper(reference_wrapper&& wr) : t(wr.t)
		{
			wr.t = nullptr;
		}

		reference_wrapper(const T& t_)
		{
			t = &t_;
		}

		reference_wrapper(T& t_)
		{
			t = &t_;
		}
                
		reference_wrapper(T&& t_)
		{
			t = &t_;
		}


		operator T& ()
		{
			return *t;
		}

		operator const T& () const
		{
			return *t;
		}

		reference_wrapper& operator=(const reference_wrapper& wr)
		{
			t = wr.t;
			return *this;
		}

		reference_wrapper& operator=(reference_wrapper&& wr)
		{
			t = wr.t;
			wr.t = nullptr;
			return *this;
		}

		reference_wrapper& operator=(T&& val)
		{
			*t = val;
			return *this;
		}
		reference_wrapper& operator=(const T& val)
		{
			*t = val;
			return *this;
		}
		reference_wrapper& operator=(T& val)
		{
			*t = val;
			return *this;
		}


	};

}
