#pragma once
#include "tuple.h"
#include <iostream>
namespace My{
	template<>
	class MyTuple<>
	{
		std::nullptr_t p = nullptr;
		const unsigned int _size = 0;
	public:

		template<class... Params>
		friend class MyTuple;

		typedef nullptr_t TopType;
		typedef MyTuple<> RestType;
		inline const unsigned int size() const
		{
			return _size;
		}
		MyTuple()
		{
			p = nullptr;
		}


		MyTuple(const MyTuple& t) = default;

		MyTuple(MyTuple&& t) = default;
		const MyTuple<>& get_rest() const
		{
			return *this;
		}
		MyTuple& get_rest()
		{
			return *this;
		}

		const nullptr_t& Top() const
		{
			return p;
		}
		nullptr_t& Top()
		{
			return p;
		}


		MyTuple& operator=(MyTuple<>&& var)
		{
			return *this;
		}

//		MyTuple& operator=(MyTuple<>& var)
//		{
//			return *this;
//		}
                MyTuple& operator=(const MyTuple& var)
                {
                      return *this;
                }

	};
        inline std::ostream& operator<<(std::ostream& s, const MyTuple<>& tpl)
        {
          s << "empty_tuple";
          return s;
          
        }

}
