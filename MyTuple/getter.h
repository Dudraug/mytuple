#pragma once
#include "tuple.h"
namespace My {
    template<int I, class... Args_>
      struct Getter;
      
    template<int I, class T, class... Args>
      struct Getter<I,T,Args...> {
        static inline auto get(MyTuple<T,Args...>& r)
        {
           return Getter<I-1,Args...>::get(r.get_rest());
        }
      };
      
      template<int I>
      struct Getter<I> {
        static inline auto get(MyTuple<>& r)
        {
           return r;
        }
      };

      template<class T, class... Args>
      struct Getter<0,T,Args...> {
         static inline auto get(MyTuple<T,Args...>& r)
         {
           return r.Top();
         }
      };

     template<int I, class... Args>
     auto inline get(MyTuple<Args...>& r)
     {
       return Getter<I,Args...>::get(r);
     }
}
