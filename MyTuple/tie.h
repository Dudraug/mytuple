#pragma once

#include "tuple.h"
#include <type_traits>
#include "reference_wrapper.h"
namespace My {
  template<class... Args>
  auto inline tie(Args&... args) 
  {
    return MyTuple<Args&...>(args...);
  }
}
