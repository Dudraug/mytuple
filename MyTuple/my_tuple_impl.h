#pragma once
#include "Helper.h"
#include "unit_tuples_common.h"
#include "tuple.h"


namespace My {


	template<class T, class... Args>
	class MyTuple<T, Args...>
	{
		typedef typename GetRefType<T>::type _T;

	    _T _val;
		MyTuple<Args...> _tuple;
		const int _size = _tuple._size + 1;
		
	public:
		typedef typename std::remove_reference<T>::type _RawT;
		typedef T TopType;
		typedef MyTuple<Args...> RestType;

		template<class First, class Second>
		friend	typename GetTPLType<First, Second>::type UnitTwoTuples(First&& first, Second&& second);

		template<class... Params>
		friend class MyTuple;

		constexpr inline const unsigned int size() const
		{
			return _size;
		}

		MyTuple() = default;

		template<class First, class Second, class... Other>
		MyTuple(First&& f, Second&& s, Other&&... r) : _val(std::forward<First>(f)), _tuple(std::forward<Second>(s), std::forward<Other>(r)...)
		{

		}

		MyTuple(const _RawT& t) :_val(t), _tuple()
		{

		}

		MyTuple(_RawT& t) :_val(t), _tuple()
		{

		}

		MyTuple(_RawT&& t) :_val(std::move(t)), _tuple()
		{

		}


		inline MyTuple<Args...>& get_rest()
		{
			return _tuple;
		}

		inline const MyTuple<Args...>& get_rest() const
		{
			return _tuple;
		}



		MyTuple(MyTuple&& tpl) : _val(std::move(tpl._val)), _tuple(std::move(tpl._tuple))
		{
			std::cout << "move" << std::endl;
		}


		MyTuple(const MyTuple& tpl) : _val(tpl._val), _tuple(tpl._tuple)
		{
			std::cout << "copy" << std::endl;
		}




		//  template<class OneTupleParam>
		//  MyTuple(MyTuple<OneTupleParam>& tpl) : _val(tpl.Top()), _tuple(tpl.get_rest()) {}



		inline const T& Top() const
		{
			return _val;
		}
		inline T& Top()
		{
			return _val;
		}
		inline MyTuple& operator=(MyTuple&& var)
		{
			_val = std::move(var._val);
			_tuple = std::move(var._tuple);
			return *this;
		}
              
		inline MyTuple& operator=(const MyTuple& var)
		{
			_val = var._val;
			_tuple = var._tuple;
			return *this;
		}

		template<class... CopyTypes>
		inline MyTuple& operator=(const MyTuple<CopyTypes...>& var)
		{
			_val = var._val;
			_tuple = var._tuple;
			return *this;
		}

                

	};


        



}


