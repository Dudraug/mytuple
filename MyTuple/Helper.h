#pragma once
#include "tuple.h"
#include "my_tuple_impl.h"
#include "reference_wrapper.h"
namespace My {
	template<class... Args_>
	class MyTuple;


	template<class... Args>
	struct GetAddTopType
	{
		typedef std::nullptr_t type;
	};


	template<class... Args>
	struct GetTPLType2 {
		typedef std::nullptr_t type;
	};
	template<class... Args, class... Args2>
	struct GetTPLType2<MyTuple<Args...>, MyTuple<Args2...>> {
		typedef MyTuple<Args..., Args2...> type;
	};

	template<class... Args, class... Args2>
	struct GetTPLType2<MyTuple<Args&...>, MyTuple<Args2&...>> {
		typedef MyTuple<Args&..., Args2&...> type;
	};

	template<class... Args>
	struct GetTPLType {
		typedef typename GetTPLType2<typename std::remove_reference<Args>::type...>::type type;
	};

	template<class T, class... Args>
	struct GetAddTopType<T&, MyTuple<Args&...>> {
		typedef MyTuple<T&, Args&...> type;
	};

	template<class T, class... Args>
	struct GetAddTopType<T, MyTuple<Args...>> {
		typedef MyTuple<typename std::remove_reference<T>::type, typename std::remove_reference<Args>::type...> type;
	};

	template<class... Args, class... Args2>
	struct GetAddTopType<MyTuple<Args...>, MyTuple<Args2...>> {
		typedef std::nullptr_t type;
	};

	template<class... Args, class... Args2>
	struct GetAddTopType<MyTuple<Args...>&, MyTuple<Args2...>> {
		typedef std::nullptr_t type;
	};

	template<class... Args, class... Args2>
	struct GetAddTopType<MyTuple<Args...>&&, MyTuple<Args2...>> {
		typedef std::nullptr_t type;
	};

	template<class F, class S>
	struct GetAddTop
	{
		typedef typename GetAddTopType<F, typename std::remove_reference<S>::type>::type type;
	};
    
	template<class T>
	struct GetRefType
	{
		typedef T type;
	};

	template<class T>
	struct GetRefType<T&> {
		typedef My::reference_wrapper<T> type;
	};

	template<class T>
	struct GetMoveType {
		typedef T&& type;
	};

	template<class T>
	struct GetMoveType<T&> {
		typedef T& type;
	};

	

	
}