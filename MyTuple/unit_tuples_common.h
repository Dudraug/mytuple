#pragma once

#include "tuple.h"
#include "Helper.h"
#include "empty_tuple.h"
#include "my_tuple_impl.h"
namespace My {


	template<class TopClass, class Second>
	inline typename GetAddTop<TopClass, Second>::type AddTupleTop(TopClass&& top, Second&& tuple);

	template<class Second>
	inline Second&& UnitTwoTuples(MyTuple<>& t, Second&& t2)
	{
		return(std::forward<Second>(t2));
	}


	template<class Second>
	inline Second&& UnitTwoTuples(MyTuple<>&& t, Second&& t2)
	{
		return(std::forward<Second>(t2));
	}
        

	template<class First, class Second>
	inline auto UnitTuplesHelper(First&& tpl1, Second&& tpl2)
	{
		return UnitTwoTuples(std::forward<First>(tpl1), std::forward<Second>(tpl2));
	}




	template<class First, class Second, class... Other>
	inline auto UnitTuplesHelper(First&& tpl1, Second&& tpl2, Other&&... other)

	{ 
		return UnitTwoTuples(std::forward<First>(tpl1), UnitTuplesHelper(std::forward<Second>(tpl2), std::forward<Other>(other)...));
	}



	template<class... Args>
	inline auto UnitTuples(Args&&... args)
	{
		return (UnitTuplesHelper(std::forward<Args>(args)...));
	}


	template<class TopClass, class Second>
	inline typename GetAddTop<TopClass, Second>::type AddTupleTop(TopClass&& top, Second&& tuple)
	{
		typedef typename  GetAddTop<TopClass, Second>::type _R;
		return _R(std::forward<TopClass>(top), std::forward<Second>(tuple));
	}

	template<class First, class Second>
	inline typename GetTPLType<First, Second>::type UnitTwoTuples(First&& first, Second&& second)
	{
		typedef typename std::remove_reference<First>::type FirstType;
		if (std::is_rvalue_reference<First&&>::value) {
			typedef typename GetMoveType<typename FirstType::TopType>::type _Move;
			return AddTupleTop(static_cast<_Move>(first._val), UnitTwoTuples(std::move(first._tuple), std::forward<Second>(second)));
		}
		
		typedef typename FirstType::_RawT& _TopType;
		typedef typename FirstType::RestType& _RestType;
		return AddTupleTop(static_cast<_TopType>(first._val), UnitTwoTuples(static_cast<_RestType>(first._tuple), std::forward<Second>(second)));
		//    	return AddTupleTop(first.Top(), UnitTwoTuples(first.get_rest(), std::forward<Second>(second)));

	}

}
