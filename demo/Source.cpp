#include <iostream>
#include <mutex>
#include <thread>
#include <map>
#include <atomic>
#include <chrono>
#include <type_traits> 
#include <vector>
#include <tuple>
#include <cstddef>
#include <stddef.h>
#include <string>
#include <functional>
#include "MyTuple.h"
typedef std::nullptr_t nullptr_t;






int main()
{
	using namespace My;
	MyTuple<std::string> tpl(std::string("1"));
	MyTuple<std::string, int> tpl2(std::string("2"), 4);
	MyTuple<std::string, int> tpl3(std::string("2"), 4);
	std::string str;
	int i;
	auto m1 = My::tie(str, i);
	auto m2 = My::tie(str, i);
	auto ref_u = My::UnitTuples(m1, m2);
	auto test = UnitTuples(tpl3, std::move(tpl), tpl2,tpl2);


	return 0;
}
